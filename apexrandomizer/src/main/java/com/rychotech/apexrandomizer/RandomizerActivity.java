package com.rychotech.apexrandomizer;

import static com.rychotech.apexrandomizer.objects.AmmoType.ARROW;
import static com.rychotech.apexrandomizer.objects.AmmoType.DROP;
import static com.rychotech.apexrandomizer.objects.AmmoType.ENERGY;
import static com.rychotech.apexrandomizer.objects.AmmoType.HEAVY;
import static com.rychotech.apexrandomizer.objects.AmmoType.LIGHT;
import static com.rychotech.apexrandomizer.objects.AmmoType.SHOTGUN;
import static com.rychotech.apexrandomizer.objects.AmmoType.SNIPER;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.rychotech.apexrandomizer.objects.Weapon;
import com.rychotech.apexrandomizer.objects.WeaponClass;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class RandomizerActivity extends AppCompatActivity {

    private static final List<Weapon> WEAPONS = Arrays.asList(
            new Weapon("Havoc", ENERGY, WeaponClass.AR),
            new Weapon("VK-47 Flatline", HEAVY, WeaponClass.AR),
            new Weapon("Hemlock", HEAVY, WeaponClass.AR),
            new Weapon("R-301 Carbine", LIGHT, WeaponClass.AR),
            new Weapon("Prowler", HEAVY, WeaponClass.SMG),
            new Weapon("R-99", LIGHT, WeaponClass.SMG),
            new Weapon("Volt", ENERGY, WeaponClass.SMG),
            new Weapon("Devotion", ENERGY, WeaponClass.LMG),
            new Weapon("L-Star", ENERGY, WeaponClass.LMG),
            new Weapon("Rampage", HEAVY, WeaponClass.LMG),
            new Weapon("G7 Scout", LIGHT, WeaponClass.MARKSMAN),
            new Weapon("30-30 Repeater", HEAVY, WeaponClass.MARKSMAN),
            new Weapon("Bocek Bow", ARROW, WeaponClass.MARKSMAN),
            new Weapon("Charge Rifle", SNIPER, WeaponClass.SNIPER),
            new Weapon("Longbow", SNIPER, WeaponClass.SNIPER),
            new Weapon("Sentinel", SNIPER, WeaponClass.SNIPER),
            new Weapon("EVA-8", SHOTGUN, WeaponClass.SHOTGUN),
            new Weapon("Mastiff", SHOTGUN, WeaponClass.SHOTGUN),
            new Weapon("Peacekeeper", SHOTGUN, WeaponClass.SHOTGUN),
            new Weapon("Mozambique", SHOTGUN, WeaponClass.SHOTGUN),
            new Weapon("RE-45", LIGHT, WeaponClass.PISTOL),
            new Weapon("P2020", LIGHT, WeaponClass.PISTOL),
            new Weapon("Wingman", HEAVY, WeaponClass.PISTOL),
            new Weapon("Alternator", DROP, WeaponClass.SMG),
            new Weapon("Spitfire", DROP, WeaponClass.LMG),
            new Weapon("Tripletake", DROP, WeaponClass.MARKSMAN),
            new Weapon("Kraber", DROP, WeaponClass.SNIPER));

    private TextView randomizerText;
    private CheckBox assaultRifleCB;
    private CheckBox marksmanCB;
    private CheckBox shotgunCB;
    private CheckBox sniperCB;
    private CheckBox subMachineGunCB;
    private CheckBox lightMachineGunCB;
    private CheckBox pistolCB;

    private Map<CheckBox, WeaponClass> weaponClassMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_randomizer);
        randomizerText = findViewById(R.id.randomizerTV);
        assaultRifleCB = findViewById(R.id.assaultRifleCB);
        marksmanCB = findViewById(R.id.marksmanCB);
        shotgunCB = findViewById(R.id.shotgunCB);
        sniperCB = findViewById(R.id.sniperCB);
        subMachineGunCB = findViewById(R.id.subMachineGunCB);
        lightMachineGunCB = findViewById(R.id.lightMachineGunCB);
        pistolCB = findViewById(R.id.pistolCB);

        weaponClassMap = new HashMap<>();
        weaponClassMap.put(assaultRifleCB, WeaponClass.AR);
        weaponClassMap.put(marksmanCB, WeaponClass.MARKSMAN);
        weaponClassMap.put(shotgunCB, WeaponClass.SHOTGUN);
        weaponClassMap.put(sniperCB, WeaponClass.SNIPER);
        weaponClassMap.put(subMachineGunCB, WeaponClass.SMG);
        weaponClassMap.put(lightMachineGunCB, WeaponClass.LMG);
        weaponClassMap.put(pistolCB, WeaponClass.PISTOL);
    }

    public void randomize(View v) {
        randomizerText.setText(retrieveRandomWeapon().map(Weapon::getName).orElse("No Weapon Class Selected"));
    }

    private Optional<Weapon> retrieveRandomWeapon() {
        List<Weapon> activeWeapons = retrieveActiveWeapons();
        if (!activeWeapons.isEmpty()) {
            return Optional.of(activeWeapons.get(new Random().nextInt(activeWeapons.size())));
        }
        return Optional.empty();
    }

    private List<Weapon> retrieveActiveWeapons() {
        return WEAPONS.stream()
                .filter(ACTIVE_WEAPONS)
                .filter(NO_DROPS)
                .filter(NOT_CURRENT_WEAPON)
                .collect(Collectors.toList());
    }


    private final Predicate<Weapon> ACTIVE_WEAPONS = weapon -> activeWeaponClasses().contains(weapon.getWeaponClass());

    private static final Predicate<Weapon> NO_DROPS = weapon -> !DROP.equals(weapon.getAmmoType());

    private final Predicate<Weapon> NOT_CURRENT_WEAPON = weapon -> !weapon.getName().equals(randomizerText.getText().toString());

    private List<WeaponClass> activeWeaponClasses() {
        return weaponClassMap.entrySet().stream()
                .filter(IS_CHECKED)
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    private static final Predicate<Map.Entry<CheckBox, WeaponClass>> IS_CHECKED = checkBoxWeaponClassEntry -> checkBoxWeaponClassEntry.getKey().isChecked();
}
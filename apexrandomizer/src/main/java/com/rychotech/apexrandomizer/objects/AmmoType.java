package com.rychotech.apexrandomizer.objects;

public enum AmmoType {
    LIGHT,
    HEAVY,
    ENERGY,
    SHOTGUN,
    SNIPER,
    ARROW,
    DROP
}

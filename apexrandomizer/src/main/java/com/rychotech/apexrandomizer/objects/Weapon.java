package com.rychotech.apexrandomizer.objects;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Weapon {
    private String name;
    private AmmoType ammoType;
    private WeaponClass weaponClass;
}

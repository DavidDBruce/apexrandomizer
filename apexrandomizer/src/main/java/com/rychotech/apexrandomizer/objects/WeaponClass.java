package com.rychotech.apexrandomizer.objects;

import lombok.Getter;

@Getter
public enum WeaponClass {
    AR("Assault Rifle"),
    SMG("Sub Machine Gun"),
    LMG("Light Machine Gun"),
    SHOTGUN("Shotgun"),
    MARKSMAN("Marksman"),
    SNIPER("Sniper"),
    PISTOL("Pistol");

    private final String name;

    WeaponClass(String name) {
        this.name = name;
    }
}
